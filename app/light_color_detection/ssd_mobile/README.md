# Light Color Detection

## Prérequis

- CUDA 10.0 
- Cudnn pour CUDA 10.0
- Carte Graphique NVIDIA

## Installation

### 1 - Installer Anaconda

Pour lancer le programme light_color_detection, il faut installer Anaconda qui permet d'avoir un environnement virtuel python.

### 2 - Importer l'env tfgpu.yml 

Pour import l'environnement 

```
conda env create -f tfgpu.yml
```

### 3 - Lancer le script 

```
python detection_video.py
```





