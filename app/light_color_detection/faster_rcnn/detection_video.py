# Import packages
import numpy as np
import os
import six.moves.urllib as urllib
import tarfile
import tensorflow as tf
from matplotlib import pyplot as plt
from PIL import Image
from os import path
from utils import label_map_util
from utils import visualization_utils as vis_util
import time
import _thread
import cv2
import sys
import vlc
from gtts import gTTS


# vocal system variables
goVoice = gTTS(text='Vous pouvez traverser!', lang='fr')
goVoice.save("good.mp3")
stopVoice = gTTS(text= 'Vous ne devez pas traverser', lang='fr')
stopVoice.save("notgood.mp3")

playerGo = vlc.MediaPlayer("good.mp3")
playerStop = vlc.MediaPlayer("notgood.mp3")


def rescale_frame(frame, percent=75):
    width = int(frame.shape[1] * percent/ 100)
    height = int(frame.shape[0] * percent/ 100)
    dim = (width, height)
    frame_resize = cv2.resize(frame, dim, interpolation =cv2.INTER_AREA)
    frame_resize = cv2.rotate(frame_resize, cv2.ROTATE_90_CLOCKWISE)
    return frame_resize

### Function To Detect Green Color
def detect_green(img, Threshold=0.01):
    """
    detect red and yellow
    :param img:
    :param Threshold:
    :return:
    """
    desired_dim = (30, 90)  # width, height
    img = cv2.resize(np.array(img), desired_dim, interpolation=cv2.INTER_LINEAR)
    img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)



    low_green = np.array([20, 30, 30])
    high_green = np.array([80, 255, 255])
    mask = cv2.inRange(img_hsv, low_green, high_green)
    cv2.imshow('Object crop hsv', mask)

    # Compare the percentage of green values
    rate = np.count_nonzero(mask) / (desired_dim[0] * desired_dim[1])

    if rate > Threshold:
        return True
    else:
        return False



### Read Traffic Light objects
# Here,we will write a function to detect TL objects and crop this part of the image to recognize color inside the object. We will create a stop flag,which we will use to take the actions based on recognized color of the traffic light.

def read_traffic_lights_object(image, boxes, scores, classes, max_boxes_to_draw=20, min_score_thresh=0.5,
                               traffic_ligth_label=1):
    im_width, im_height = image.size
    go_flag = False
    for i in range(min(max_boxes_to_draw, boxes.shape[0])):
        if scores[i] > min_score_thresh and classes[i] == traffic_ligth_label:
            ymin, xmin, ymax, xmax = tuple(boxes[i].tolist())
            (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                          ymin * im_height, ymax * im_height)
            crop_img = image.crop((left, top, right, bottom))

            if detect_green(crop_img):
                go_flag = True

    return go_flag



def main():
    # This is needed since the notebook is stored in the object_detection folder.
    sys.path.append("..")

    # Name of the directory containing the object detection module we're using
    MODEL_NAME = 'models'
    VIDEO_NAME = 'test.mp4'

    # Grab path to current working directory
    CWD_PATH = os.getcwd()

    # Path to frozen detection graph .pb file, which contains the model that is used
    # for object detection.
    PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join(CWD_PATH,'label_map.pbtxt')

    # Path to video
    PATH_TO_VIDEO = os.path.join(CWD_PATH,VIDEO_NAME)

    # Number of classes the object detector can identify
    NUM_CLASSES = 6

    # Load the label map.
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    # Load the Tensorflow model into memory.
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

    # Define input and output tensors (i.e. data) for the object detection classifier

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Output tensors are the detection boxes, scores, and classes
    # Each box represents a part of the image where a particular object was detected
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects.
    # The score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Open video file
    video = cv2.VideoCapture(PATH_TO_VIDEO)

    frameIndex = 0

    while(video.isOpened()):

        # incremente frame
        frameIndex += 1

        # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
        # i.e. a single-column array, where each item in the column has the pixel RGB value
        ret, frame = video.read()
        if frame is not None:

            frame_resize = rescale_frame(frame, percent=50)
            #frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            frame_expanded = np.expand_dims(frame_resize, axis=0)

            # Perform the actual detection by running the model with the image as input
            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: frame_expanded})

            # Return if traffic is green or not
            image = Image.fromarray(frame_resize, mode="RGB")
            go_flag = read_traffic_lights_object(image, np.squeeze(boxes), np.squeeze(scores),
                                                               np.squeeze(classes).astype(np.int32))

            # Display Go or Stop
            if go_flag:
                cv2.putText(frame_resize, 'Go', (15, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                if frameIndex % 60 == 0 :
                    playerStop.pause()
                    playerGo.play()
                    print("go " + str(frameIndex))
            else:
                cv2.putText(frame_resize, 'Stop', (15, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)                                                            
                if frameIndex % 60 == 0 :
                    playerGo.pause()
                    playerStop.play()
                    print("stop " + str(frameIndex))

            # Draw the results of the detection (aka 'visulaize the results')
            vis_util.visualize_boxes_and_labels_on_image_array(
                frame_resize,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=8,
                min_score_thresh=0.5)

            # All the results have been drawn on the frame, so it's time to display it.
            cv2.imshow('Object detector', frame_resize)
        else:
            break
        # Press 'q' to quit
        if cv2.waitKey(1) == ord('q'):
            break

    # Clean up
    video.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
